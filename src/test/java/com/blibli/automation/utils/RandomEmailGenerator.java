package com.blibli.automation.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomEmailGenerator {

    public static String generate(){
        String email = "";
        Double length = (Math.random()*10+5);
        String allowedChars = "zxcvbnmasdfghjklqwertyuiop" + "1234567890" + "_-.";
        String temp = RandomStringUtils.random(length.intValue(), allowedChars);
        email = temp + "@gmail.com";
        return email;
    }
}
