package com.blibli.automation.pages.common.popup;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.concurrent.TimeUnit;

public class Register extends PageObject {

    @FindBy(id = "gdn-register-form")
    private WebElementFacade formRegister;

    @FindBy(xpath = "//input[@placeholder='Masukan email']")
    private WebElementFacade inputEmail;

    @FindBy(xpath = "//input[@placeholder='Masukan kata sandi']")
    private WebElementFacade inputPassword;

    @FindBy(xpath = "//button[@class='button-submit']")
    private WebElementFacade btnDaftar;


    public Boolean isPopRegisterVisible(){
        return formRegister
                .withTimeoutOf(5, TimeUnit.SECONDS)
                .isCurrentlyEnabled();
    }

    public void inputEmail(String email){
        inputEmail.type(email);
    }

    public void setInputPassword(String password){
        inputPassword.type(password);
    }

    public void clickButtonDaftar(){
        btnDaftar.click();
    }



}
