package com.blibli.automation.pages.common;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class Header extends PageObject {

    @FindBy(xpath = "//a[@id='gdn-daftar']")
    private WebElementFacade btnDaftar;

    public void clickBtnDaftar(){
        btnDaftar.click();
    }


}
