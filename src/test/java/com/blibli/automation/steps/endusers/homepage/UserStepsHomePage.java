package com.blibli.automation.steps.endusers.homepage;

import com.blibli.automation.pages.common.Header;
import com.blibli.automation.pages.common.popup.Register;
import com.blibli.automation.pages.homepage.BliBliHomePage;
import com.blibli.automation.utils.RandomEmailGenerator;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class UserStepsHomePage {
    BliBliHomePage bliBliHomePage;
    Header header;
    Register register;

    @Step
    public void openHomePage(){
        bliBliHomePage.open();
    }

    @Step
    public void clickButtonDaftar(){
        header.clickBtnDaftar();
    }

    @Step
    public void isPopUpRegistershow(){
        Boolean actual = register.isPopRegisterVisible();
        assertThat("pop up is not shown",actual,equalTo(true));
    }

    @Step
    public void inputEmailOnPopUpRegister(){
        register.inputEmail(RandomEmailGenerator.generate());
    }

    @Step
    public void inputPasswordOnPopUpRegister(String password){
        register.setInputPassword(password);
    }

    @Step
    public void clickButtonDaftaronPopUpRegister(){
        register.clickButtonDaftar();
    }

}
