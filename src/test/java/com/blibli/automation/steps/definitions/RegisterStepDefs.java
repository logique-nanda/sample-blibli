package com.blibli.automation.steps.definitions;

import com.blibli.automation.steps.endusers.homepage.UserStepsHomePage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class RegisterStepDefs {

    @Steps
    UserStepsHomePage userStepsHomePage;

    @Given("user open blibli.com homepage")
    public void userOpenBlibliComHomepage() {
        userStepsHomePage.openHomePage();
    }

    @When("^user click button daftar$")
    public void userClickButtonDaftar() {
        userStepsHomePage.clickButtonDaftar();
    }

    @Then("^form register user should be shown$")
    public void formRegisterUserShouldBeShown() {
        userStepsHomePage.isPopUpRegistershow();
    }

    @When("^user input email with random correct email$")
    public void userInputEmailWithRandomCorrectEmail() {
        userStepsHomePage.inputEmailOnPopUpRegister();
    }

    @And("^user input password \"([^\"]*)\"$")
    public void userInputPassword(String password)  {
        userStepsHomePage.inputPasswordOnPopUpRegister(password);
    }

    @And("^user click button daftar on pop up register$")
    public void userClickButtonDaftarOnPopUpRegister() {
        userStepsHomePage.clickButtonDaftaronPopUpRegister();
    }

    @Then("^the user should redirected int pnv page$")
    public void theUserShouldRedirectedIntPnvPage() {
        // userStepsHomePage.isAlreadyLogin();

    }

    @When("^user click verifikasi nanti$")
    public void userClickVerifikasiNanti() {

    }

    @Then("^pop up verifikasi nanti should be apper$")
    public void popUpVerifikasiBabtiShouldBeApper() {
    }

    @When("^user click lanjutkan on pop up verifikasi nanti$")
    public void userClickLanjutkanOnPopUpVerifikasiNanti() {
    }

    @Then("the user should successfully login into blibli.com")
    public void theUserShouldSuccessfullyLoginIntoBlibliCom() {
    }

    @And("^tampil \"([^\"]*)\" pada header pop up$")
    public void tampilPadaHeaderPopUp(String header) throws Throwable {

        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
