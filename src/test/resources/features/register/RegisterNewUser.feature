@RegisterNewUserFeature
Feature: Register New User

  @Positive @Regression
  Scenario: Register new user with correct email address
    Given user open blibli.com homepage
    When user click button daftar
    Then form register user should be shown
    And tampil "ukuran" pada header pop up
    When user input email with random correct email
    And user input password "testqwerty123"
    And user click button daftar on pop up register
#    Then the user should redirected int pnv page
#    When user click verifikasi nanti
#    Then pop up verifikasi nanti should be apper
#    When user click lanjutkan on pop up verifikasi nanti
#    Then the user should successfully login into blibli.com